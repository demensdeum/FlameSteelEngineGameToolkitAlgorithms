#ifndef FLAMESTEELENGINEGAMETOOLKITFADEVISUALEFFECTTYPE_H_
#define FLAMESTEELENGINEGAMETOOLKITFADEVISUALEFFECTTYPE_H_


namespace FlameSteelEngine {
namespace GameToolkit {
namespace Controllers {
namespace FadeVisualEffectAnimation {

enum Type {
	fadeInType,
	fadeOutType,
};

};
};
};
};

#endif