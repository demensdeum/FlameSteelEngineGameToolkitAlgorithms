#ifndef FLAMESTEELENGINEGAMETOOLKITALGORITHMSFADEVISUALEFFECTCONTROLLER_H_
#define FLAMESTEELENGINEGAMETOOLKITALGORITHMSFADEVISUALEFFECTCONTROLLER_H_

#include <memory>
#include "FadeVisualEffectState.h"
#include "FadeVisualEffectAnimationType.h"

using namespace std;

namespace FlameSteelCore {
class Object;
class Objects;
};

namespace FlameSteelEngine {
namespace GameToolkit {
namespace Controllers {

class FadeVisualEffectControllerDelegate;

using namespace FlameSteelCore;
using namespace FlameSteelEngine::GameToolkit::Controllers::FadeVisualEffectAnimation;

class FadeVisualEffectController: public enable_shared_from_this<FadeVisualEffectController> {

public:
	void addObject(shared_ptr<Object> object);
	void removeAllObjects();

	void startFadeIn();
	void startFadeOut();

	void step();

	weak_ptr<FadeVisualEffectControllerDelegate> delegate;

private:
	State state = idleState;
	shared_ptr<Objects> objects = make_shared<Objects>();
	int animationCounter = 0;

	void performFadeAnimation(Type animationType);

};

};
};
};

#endif