#ifndef FLAMESTEELENGINEGAMETOOLKITFADEVISUALEFFECTSTATE_H_
#define FLAMESTEELENGINEGAMETOOLKITFADEVISUALEFFECTSTATE_H_

namespace FlameSteelEngine {
namespace GameToolkit {
namespace Controllers {
namespace FadeVisualEffectAnimation {

enum State {

	fadeInState,
	idleState,
	fadeOutState,

};

};
};
};
};

#endif