#ifndef FLAMESTEELENGINEGAMETOOLKITALGORITHMSFADEVISUALEFFECTCONTROLLERDELEGATE_H_
#define FLAMESTEELENGINEGAMETOOLKITALGORITHMSFADEVISUALEFFECTCONTROLLERDELEGATE_H_

#include <memory>
#include "FadeVisualEffectAnimationType.h"

namespace FlameSteelCore {
class Object;
};

using namespace std;
using namespace FlameSteelCore;
using namespace FlameSteelEngine::GameToolkit::Controllers;
using namespace FlameSteelEngine::GameToolkit::Controllers::FadeVisualEffectAnimation;
namespace FlameSteelEngine {
namespace GameToolkit {
namespace Controllers {

class FadeVisualEffectController;

class FadeVisualEffectControllerDelegate {

public:
	virtual void fadeVisualEffectDidFinishAnimation(shared_ptr<FadeVisualEffectController> fadeVisualEffectController, Type animationType) = 0;
	virtual void fadeVisualEffectDidUpdateObject(shared_ptr<FadeVisualEffectController> fadeVisualEffectController, shared_ptr<Object> updatedObject) = 0;
}; 

};
};
};

#endif