#include "FadeVisualEffectController.h"
#include <FlameSteelCore/Objects.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include "FadeVisualEffectControllerDelegate.h"

#include <iostream>

using namespace std;

using namespace FlameSteelEngine::GameToolkit::Controllers;

void FadeVisualEffectController::addObject(shared_ptr<Object> object) {
	objects->addObject(object);
};

void FadeVisualEffectController::removeAllObjects() {
	objects->removeAllObjects();
};

void FadeVisualEffectController::startFadeIn() {
	if (state != idleState) {
		return;
	}
	animationCounter = 0;
	state = fadeInState;
};

void FadeVisualEffectController::startFadeOut() {
	if (state != idleState) {
		return;
	}
	animationCounter = 0;
	state = fadeOutState;
};

void FadeVisualEffectController::step() {

	switch (state) {

		case fadeInState:
			performFadeAnimation(fadeInType);
			break;

		case idleState:
			break;

		case fadeOutState:
			performFadeAnimation(fadeOutType);
			break;
	}
};

void FadeVisualEffectController::performFadeAnimation(Type animationType) {

	for (auto i =0; i < objects->size(); i ++) {
		auto object = objects->objectAtIndex(i);
		auto brightness = animationCounter == 0 ? 0.f : float(animationCounter) / 100.f;
		if (animationType == fadeOutType) {
			brightness = 1.0 - brightness;
		}
		FSEGTUtils::getObjectBrightness(object)->floatNumber = brightness;
		auto lockedDelegate = this->delegate.lock();
		if (lockedDelegate != nullptr) {
			lockedDelegate->fadeVisualEffectDidUpdateObject(shared_from_this(), object);
		}
	}

	animationCounter++;
	if (animationCounter > 100) {
		state = idleState;
		auto lockedDelegate = this->delegate.lock();
		if (lockedDelegate != nullptr) {
			lockedDelegate->fadeVisualEffectDidFinishAnimation(shared_from_this(), animationType);
		}
	}
};